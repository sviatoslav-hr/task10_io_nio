package com.khrystyna;

import com.khrystyna.service.DirectoryService;
import com.khrystyna.service.UserService;

public class Application {
    /**
     * Main method that runs when the program is started.
     *
     * @param args command-line arguments
     */
    public static void main(String[] args) {
        UserService userService = new UserService();
        userService.readCommand();
    }

}
