package com.khrystyna.service;

import java.io.File;
import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class DirectoryService {
    private File dir;

    public DirectoryService() {
        changeDirectory(".");
        try {
            changeDirectory(dir.getCanonicalPath());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    String getPath() {
        return dir.getAbsolutePath();
    }

    void changeDirectory(String path) {
        changeDirectory(new File(path));
    }

    private void changeDirectory(File file) {
        dir = file;
    }

    void backToParent() {
        changeDirectory(dir.getParentFile());
    }

    boolean changeToSubdirectory(String path) {
        List<String> listPath = Arrays.asList(path.split("\\s\\\\\\s"));
        return changeToSubdirectoryOf(dir, listPath);
    }

    private boolean changeToSubdirectoryOf(File dir, List path) {
        for (File file : Objects.requireNonNull(dir.listFiles())) {
            if (file.getName().equals(path.get(0))) {
                if (path.size() > 1) {
                    return changeToSubdirectoryOf(file, path.subList(1, path.size()));
                } else {
                    changeDirectory(file);
                    return true;
                }
            }
        }
        return false;
    }

    List<String> getContent() {
        return Arrays.stream(Objects.requireNonNull(dir.listFiles()))
                .map(file -> file.isDirectory() ? file.getName() + File.separator : file.getName())
                .collect(Collectors.toList());
    }

    boolean createDirectory(String name) {
        File newDir = subFile(name);
        return newDir.mkdir();
    }

    boolean removeDirectory(String name) {
        return removeDirectory(subFile(name));
    }

    private boolean removeDirectory(File file) {
        if (file.isDirectory() && !isDirEmpty(file)) {
            return removeDirNotEmpty(file);
        } else {
            return file.delete();
        }
    }

    private boolean removeDirNotEmpty(File dir) {
        try {
            for (String s : Objects.requireNonNull(dir.list())) {
                File currentFile = new File(dir.getPath(), s);
                removeDirectory(currentFile);
            }
            return removeDirectory(dir);
        } catch (Exception ex) {
            return false;
        }
    }

    private File subFile(String name) {
        return new File(getPath(), name);
    }

    private boolean isDirEmpty(final File directory) {
        Path path = directory.toPath();
        try (DirectoryStream<Path> dirStream = Files.newDirectoryStream(path)) {
            return !dirStream.iterator().hasNext();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }
}
