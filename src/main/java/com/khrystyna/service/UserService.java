package com.khrystyna.service;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Scanner;
import java.util.regex.Pattern;

public class UserService {
    private static Logger logger = LogManager.getLogger(UserService.class);
    private DirectoryService directoryService = new DirectoryService();
    private Scanner scanner = new Scanner(System.in);

    public void readCommand() {
        System.out.print(directoryService.getPath() + " > ");
        processCommand(scanner.nextLine());
    }

    private void processCommand(String command) {
        String[] strings = command.trim().split("(\\s)+");
        if (strings.length == 0) {
            readCommand();
        } else {
            String mainCommand = strings[0];
            String params = command
                    .replaceFirst(Pattern.quote(mainCommand), "").trim();
            switch (mainCommand) {
                case "cd":
                    changeDirectory(params);
                    break;
                case "dir":
                    showDirContent();
                    break;
                case "mkd":
                    createDirectory(params);
                    break;
                case "rmd":
                    removeDirectory(params);
                    break;
                case "exit":
                    return;
                default:
                    logger.warn("Unknown command \'" + mainCommand + '\'');
            }
            readCommand();
        }
    }

    private void removeDirectory(String params) {
        String name = getFirstParam(params);
        boolean success = directoryService.removeDirectory(name);
        if (success) {
            logger.trace("Success!");
        } else {
            logger.warn("During removing error happened!");
        }
    }

    private void createDirectory(String params) {
        String name = getFirstParam(params);
        boolean success = directoryService.createDirectory(name);
        if (!success) {
            logger.warn("Can not create the directory with the name \'" + name + '\'');
        }
    }

    private void changeDirectory(String params) {
        String path = getFirstParam(params);
        String[] strings = path.split("(\\\\)+");
        if (strings[0].matches("[a-zA-Z]:\\\\")) {
            directoryService.changeDirectory(path);
        } else if (strings[0].equals("..")) {
            directoryService.backToParent();
        } else {
            boolean res = directoryService.changeToSubdirectory(path);
            if (!res) {
                logger.warn("Incorrect path specified!");
            }
        }
    }

    private void showDirContent() {
        for (String s : directoryService.getContent()) {
            logger.trace(s);
        }
    }

    private String getFirstParam(String params) {
        int end;
        if (params.startsWith("\"")) {
            params = params.substring(1);
            end = params.indexOf('\"');
        } else {
            if (params.contains(" ")) {
                end = params.indexOf(' ');
            } else {
                end = params.length();
            }
        }
        return params.substring(0, end);
//        params = params.substring(end).trim();
    }
}
